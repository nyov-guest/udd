#!/bin/sh -e
TARGETDIR=/srv/udd.debian.org/mirrors/i18n-apps
mkdir -p $TARGETDIR
rm -rf $TARGETDIR/*

# work around a regression in wget from wheezy to jessie
# see https://wiki.debian.org/ServicesSSL#wget
WGET=wget
dir=/etc/ssl/ca-debian
test -d $dir && WGET="wget --ca-directory=$dir"

$WGET -q http://i18n.debian.org/material/data/unstable.gz -O ${TARGETDIR}/sid.gz
$WGET -q http://i18n.debian.org/material/data/testing.gz -O ${TARGETDIR}/wheezy.gz

